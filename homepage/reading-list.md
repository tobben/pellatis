---
layout: page
title: Reading List
permalink: /reading-list/
---

### Technical Stuff
There exists something called a barrier screw, that we could try to manufacture if we want to save some weight.
[Barrier screws patent history](https://www.ptonline.com/articles/no-14---barrier-screws).

[Understanding Screw Design for Film Extrusion Process](http://www.macroeng.com/understanding-screw-design-for-film-extrusion-process.php) establishes some more useful
vocabulary.

[Wikipedia Plastic Extrusion](https://en.wikipedia.org/wiki/Plastic_extrusion) has some good information.

### Notes on the Freecad files in the repo
* Freecad is free software and can be downloaded [here](https://www.freecadweb.org/downloads.php)
* From freecad you can export part files in both .stl and .step format
* The parts that you need to fabricate yourself is in the folder called "Parts to fabricate". The store bought parts that you need to get from robotdigg and other vendors are in the folder called "Store bought parts".

![Parts-in-Freecad](/pellatis/img/cad-model-of-robotdigg-parts.png)

### Sibling Projects

**FabMX**

Their development site [here](https://wiki.fablab-muenchen.de/display/FABMX/Pellet+Extruder).

They are planning to build a metal 3D printer that will extrude from pellets of something called MIM feedstock.
See [presentation video](https://www.youtube.com/watch?v=BP5TD_diqUk).

They have milled a compression screw: [tweet](https://twitter.com/ProjectFabMX/status/1292913647153487874).

**Atte Linna (Badass Pellet Extruder)**

Brief introduction and some discussion is published in a [reprap forum thread](https://reprap.org/forum/read.php?423,864309).

They seem to have bought something like [this $293 (incl ship, excl vat) compression
screw](https://www.ebay.com/itm/16-or-20mm-Dia-Extruder-Screw-Barrel-with-1-75-3mm-Nozzle-for-Desktop-Extruder/352752511733?hash=item5221b02af5:g:4FQAAOSwB9FdXOpR).
It has 16mm dia, and ships with barrel and nozzle.

**Michigan Open Sustainability Lab**

Published a [report](https://www.mdpi.com/2411-5134/5/3/26/htm) on how to make a compression screw with a grinder and a custom lathe.
See [hackaday article](https://hackaday.com/2020/07/09/open-source-grinder-makes-compression-screws-for-plastic-extruders-easy/).


**Gigabot-Labs Pellet-Extruder**

Revision history in their repo [here](https://github.com/Gigabot-Labs/Pellet-Extruder/wiki/Revision-history).

**Mahor**

### Commercially Available Closed Options

[Dyze homepage](https://dyzedesign.com/)
[Dyze Tom's](https://www.youtube.com/watch?v=eeYgj3ZIJjA&t=602s)

[Massive Dimension](https://3dprintingindustry.com/news/massive-dimension-releases-pellet-extruder-for-large-format-3d-printing-146457/)

[Rev3rd MD-;10](https://rev3rd.com/pellet-extruder/)

### General

 - [ScienceDirect Screw Extruder Article](https://www.sciencedirect.com/topics/engineering/screw-extruder)
