---
layout: page
title: Design files
permalink: /design-files/
---

# The software

We have designed all part for Pellatis in Freecad, free open source CAD software you can download [here](https://www.freecadweb.org/downloads.php).

# The files

The design of the first iteration of Pellatis in development. You can download the latest version [here](https://gitlab.com/tobben/pellatis/-/raw/master/extruder-master-document.FCStd?inline=false) or look up the files in the Gitlab [repo](https://gitlab.com/tobben/pellatis).

# Exporting parts

To export parts for 3D printing or machining, select the part and go to *-file -export*.

![](/pellatis/img/extruder-hopper-adapter.PNG)
