---
layout: page
title: Bill Of Materials
permalink: /BOM/
---

The stuff you need to buy in order to make your own Pellatis

**Robotdigg:**

* Auger (screw), barrel and nozzle package, option "E16SB300" [1pcs](https://www.robotdigg.com/product/1691/16mm-or-20mm-extruder-screw,-barrel-n-nozzle) *210USD + shipping*
* Mica band heater 35mm, option "3540-220V135W" [2pcs](https://www.robotdigg.com/product/1498/Mica-Band-Heater-for-Extrusion-or-Injection-Molding-Machine) *3,60 USD pluss shipping*
* NMRV30 1:10 worm gearbox, option "NMRV30" [1pcs](https://www.robotdigg.com/product/1452/RV30,-RV40,-RV50-or-RV63-worm-gear-reducer) *29 USD pluss shipping*
* 8mm input shaft adapter sleeve, option "SS-RV30-8" [1pcs](https://www.robotdigg.com/product/1452/RV30,-RV40,-RV50-or-RV63-worm-gear-reducer) *3,60 USD pluss shipping*
* SSR-10DA (solid state relay), [2pcs](https://www.robotdigg.com/product/589/10A,-40A-DC-AC-or-15A-AC-AC-Solid-state-Relay), *10,20 USD plus shipping*
* HSSSR (Heatsink for SSR), [2pcs](https://www.robotdigg.com/product/489), *1,60 USD plus shipping. Might not be strictly necessary but we recommended these anyway to be safe.*

**E3D:**

* High Precision Heater Cartridges [2pcs](https://e3d-online.com/collections/printer-parts-electrical/products/high-precision-heater-cartridges) *40,8 GBP + shipping*
* 104GT-2 Semitech therminstor, shape: Thermistor Cartridge, [3pcs](https://e3d-online.com/collections/printer-parts-electrical/products/thermistor-cartridge) *£13,5 (ca 18,2 USD) plus shipping*

**Amazon:**

* Heatsinks with fan above/around feed zone [3pcs](https://www.amazon.com/Readytosky-Brushless-Heatsink-Cooling-Control/dp/B08CR673JP/ref=sr_1_7?dchild=1&keywords=rc%2Bmotor%2Bheatsink%2B3550&qid=1608217971&sr=8-7&th=1) *30 USD plus shipping*

**Other vendors:**

* Maybe some glue to better fasten thermistors, [1pcs](https://www.biltema.se/bilvard/bilunderhall/skyddstatningsmedel/flanstatning-2000040362) *ca $10*


**Not yet determined shops/product types:**

* Nema23 closed stepper motor and driver
* Nema17 open stepper for lightly moving around (evenly distributing/"whipping") pellets in hopper
* Screws
* Aluminium or other material for machined mounting frame
* Thermal barrier sheet (cardboard)?
* Electronics enclosure for contactors / power managment?
* Cables for power and signals
* Plugs for power and signal cables for fast attachment detachment?
