---
layout: page
title: About
permalink: /about/
---

# Pellatis

A pellet extruder that fits The Hangprinter and Fabricatable Machines projects.

## Introduction
A good pellet extruder can be a game changer for The Hangprinter and Fabricatable Machines projects.
It might solve three issues:

 * Filament cost
 * Friction in the recycling process
 * Print/melt speeds

## Background
There already exist many open source projects related to pellet extrusion.
Our intention is to put together bits and pieces, to make something that
fits The Fabricatable Machines Project's needs and design goals,
and then to scale that down to fit the Hangprinter Project's goals later.

### Hangprinter Specific Needs

See [this link](https://torbjornludvigsen.com/blog/2018/#hangprinter_project_59) for more on HP design goals.

I think it would be best for HP to take small steps upwards from the SuperVolcano.
 - Throughput: 300-500 g/h?
 - Size: Doesn't matter much, we can create an effector that fits.
 - Weight: prioritized. Lower is better.
 - Nozzle size: 3-5 mm?
 - Price: below £1000 would be nice.

High priority would be ability to control drag forces on the effector.
If this is uneven or unpredictable we'll have visible artifacts, as lines/frame are never perfectly stiff.
Self-reproducibility would also be prioritized as this is still the #1 design principle of HP.
