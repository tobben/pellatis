---
layout: post
title:  "New design tweaks"
date:   2021-08-30 14:02:14 +0100
categories: dev update
---
_by Jens Dyvik_


![](/pellatis/img/tobben-spoon-feeding.JPG)
*Torbjørn spoon feeding the extruder for maximum pellet eating speed insights*

This summer Torbjørn and I got to meet in person again and do some more testing together of the first Pellatis prototype. Long story short, despite testing much higher temps *(190C)* and other tricks, we still got a clogged extrusion screw.

# New design tweaks

With further research, discussions and input from fiends we have come up with the following design improvements:

 - Create a heat break between the inlet and first heat zone
 - Add an extra band heater above the first thermistor cartridge *(it will be driven in parallel with the band heater bellow the first thermistor)*
 - Hack the heatsinks so that they can fit around the hopper inlet *(inspired by the [Bloft Bad ass pellet extruder v1](https://github.com/Bloft-Design-Lab/Bad-Ass-Pellet-Extruder))*
 - Add some thermal paste between the barrel and the heatsinks. The barrel is quite roughly turned, so this should help cooling performance.

![](/pellatis/img/heat-break-dev-and-extra-heater.jpg)
*This screenshot shows the design changes from the current version*

![](/pellatis/img/heat-break-dev-and-extra-heater-detail.jpg)
*Close-up of the new approach*

# Desired improvements

 - Pellets should start melting sooner and faster, thereby reduce clogging.
 - There should be about two flights of auger from the inlet to the end of the heat break/start of the first heating zone. This is a the conventional approach in screw based thermoplastic extrusion.
 - 135W extra power from the extra band heater should bring total power from 350W to 485W, thereby hopefully improve future productivity performance.
 
 Next up is machining the heatbreak groove in the barrel, printing the new hopper adapter and hack the the heatsinks









