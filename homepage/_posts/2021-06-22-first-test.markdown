---
layout: post
title:  "First test extrusions and troubleshooting"
date:   2021-06-22 14:02:14 +0100
categories: dev update
---
_by Jens Dyvik_

TLDR; The extrusion screw clogged and we are trying to figure out which temp settings to use *(and which design tweaks to make)*

# A slow hello world

![](/pellatis/img/test1-first-drip.JPG)
*We started out with brand new PLA pellets and a very slow rpm on the auger. After about 25 minutes plastic started coming out, but the flow quickly stopped*

# A new hopper adapter

![](/pellatis/img/test1-second-hopper-adapter.JPG)
*We designed a new hopper adapter to improve visibility to the auger during feeding of pellets. The threads printed surprisingly ok in this 45degree orientation*

![](/pellatis/img/test1-better-view-to-auger.JPG)
*Much easier to see the pellets rolling in now*

Test log thursday 10.6.2021:

 - heat zones 210 210 140 no luck *(temps in Celsius, starting from nozzle)*
 - super speed 15000mms no luck
 - heat zones 190 190 140 no luck
 - heat zones 210 210 180 no luck

![](/pellatis/img/test1-teardrop-art.JPG)
*After testing with the settings above, we only got this nest of teardrops*


# Teardown no.1

![](/pellatis/img/test1-teardown1.JPG)
*We decided to disassemble to get better insights into what might be going wrong*

![](/pellatis/img/test1-teardown1-barrel-opening.JPG)
*Not much plastic at the auger tip*


![](/pellatis/img/test1-teardown1-nozzle-opening.JPG)
*Not much in the nozzle either*

![](/pellatis/img/test1-teardown1-birth-of-an-auger.JPG)
*We had to increase the temperature of the tow band heaters to about 120c to get the screw out*


![](/pellatis/img/test1-teardown1-auger.JPG)
*The screw with different stages of melting, looks like a clog. We only did a very crude cleanup before reassembling*

# More teardrops and no fun

Test log 13.6.2021:
 
 - heat zone 190 190 60 - extruded for about 15 min no results.
 - heat zone 190 190 90 - extruded for 60 min no results. speed 10000 gave closed loop error in the end (too many lost steps)
 - heat zone 200 190 120 - speed 15000 without alarm, but sounds not super good. extruded at least another 20min with no results
 - main theory: too tight between screw end and nozzle start, molten PLA can't pass *(in hindsight this was probably not true)*



![](/pellatis/img/test1-take-two-teardrop-art.JPG)
*More tears, no proper extrusion*

# More tweaks

![](/pellatis/img/test1-removing-spacer-ring.JPG)
*We removed the spacer ring to allow for more space between the end of the screw and the taper of the nozzle. (In hindsight this was probably not so smart, because the slightly tapered end of the screw does not allow it to move too much backwards)*

![](/pellatis/img/extruder-hopper-adapter-extra-fan-mouning-combo.PNG)
*We designed a third iteration of the hopper adapter for improved pellet flow*

![](/pellatis/img/test1-third-hopper-adapter.JPG)
*Note the fillet for a little bit of extra strength*

![](/pellatis/img/test1-third-hopper-adapter-mounted-to-extra-heatsink.JPG)
*The hopper adpater doubles as a mount for a third heatsink*

Test log 14.6.2021:

 - This page has info on heat zone temps for PLA extrusion: https://www.sciencedirect.com/topics/engineering/poly-lactic-acid
 - Auger to bearing dist before spacer removal: 37.4mm
 - Removed spacer ring, so auger should be able to move back 9mm now
 - 200 190 105 - Feedzone lukewarm. motor loosing steps at 4000mm/m
 - 210 200 120 - motor loosing steps - auger still not moving backwards (0nly 0,8mm). wondering if the shaft key is jammed
 - let the extruder cool down and pulled the key back 10mm with pliers
 - 210 200 120 - kept increasing speed until 15000mm/m, started loosing steps. auger still not moving back. feed inlet lukewarm/warm
 

Test log 16.6.2021

 - 210 200 165 mid thermistor stays at 214, mid heater no on. no extrusion after 20 min of different speed test and trying to jam pellets into the screw. upper heatsinks feels ok (maybe 35c)
 - 200 195 165 no success. But the screw has moved back 2mm, so blocked nozzle is no longer a suspect
 - Chopped up larger pieces of pla (2mmX2mmx4mm) to try to release clog. Jammed the extruder. Motor can't move. I blame this guide: http://extrusionwiki.com/wiki/CC-V19-2-B.ashx
 

# More tears and teardown no.1

![](/pellatis/img/test1-teardown2-barrel-opening.JPG)
*We took apart the extruder again. The screw has moved backwards, but it became quite evident that we had a clog*

![](/pellatis/img/test1-teardown2-auger.JPG)
*The screw after the second rounds of testing. Note the damage to the shaft (left) from rubbing against the bushing.*

My main theory is that we need to cool more at the opening and heat more in the top part of the extruder (*the heater closest to the inlet)*. Tobben thinks we might have heated too much in the same zone. More testing needed. This webpage has relevant info on the temperature gradient from the inlet to the first heater on a screw extruder https://www.ptonline.com/articles/extrusion-dont-forget-zone-zero


Notes and thoughts for further testing:

 - Clean and polish screw
 - Degrease barrel inside better *(it was covered in cutting fluid or oil from manufacturing)*
 - Try a higher temp at top heater
 - Make a new spacer ring *(6mm, not 9mm)*
 - Clean damaged shaft and bushing. Apply some lubricating oil when reassembling
 - Add cooling paste to heatsinks
 - Replace top band heater with a more powerful one? *(it currently takes the top heatzone about 12 minutes to go from room temp to 165c)*
 







