---
layout: post
title:  "Hacking Store-bought Parts"
date:   2021-06-13 14:02:14 +0100
categories: dev update
---
_by Jens Dyvik_


We are aiming to make this design as simple as possible to replicate. But some of the parts still need some hacking. Fun to do, but a little tricky.

# The Barrel

We decided to be a bit proper and try to measure temperature inside the barrel, not on the outside (which would have been the easy option). Unfortunately this requires hole to be drilled for the thermistor cartridges at quite precise locations.

### Making Jig To Mill Flats and Spot

In order to locate the barrel precisely in the ShopBot at Fellesverkstedet we made a jig from leftover MDF. By having the machine mill the jig itself, registration becomes easy and fast. Basically the machine itself tells you where to place the barrel.

![](/pellatis/img/extruder-jig-skecth.PNG)
*The first jig idea was b it more complicated than necessary.*

![](/pellatis/img/a-simpler-jig.PNG)
*Using some leftover 19mm stock we could make the jig from a single peice. The trick was to have the barrel flange protrude over the edge of the shopbot.*

![](/pellatis/img/jig-milled.PNG)
*The finished jig milled. The CAD model of the jig is in the freecad assembly in the Pellatis repo*

### Milling Flats With High Speed Machining and Spotting Drill Hole Locations

![](/pellatis/img/barrel-in-jig.JPG)
*We used some leftover pipe clamps to hold the barrel down.*

![](/pellatis/img/trochoidal-toolpaths-in-bark-beetle-cam.PNG)
*We used the experimental trochoidal milling feature in Bark Beetle (open source CAM extension for Rhino developed by Jens Dyvik). You could also use Freecad or Fusion360 to set up these toolpaths.*

![](/pellatis/img/milling-flats.PNG)
*Milling in progress.*

![](/pellatis/img/flats-and-spotted.JPG)
*Finished flats with hole locations spotted, no need to precisely measure out their location on a round barrel manually.*

### Transfer Jig to Drill Press With Barrel Still Clamped

![](/pellatis/img/jig-transferred-to-drill-press.JPG)
*By leaving the barrel clamped in the jig we didn't need to worry about having the correct rotation of the barrel. By using a drillpress the drilling went smooth and easy.*

### Edge Break and Cleanup

![](/pellatis/img/machined-holes.JPG)
*The holes and their corresponding cartridges.*

***

# The Heatsinks

### Bending the Outer Wings 90 Degrees

We decided to have the heatsinks opposite each other for even cooling of the barrel. By bending the outer wings 90 degrees and drillin holes, mounting became easy. We also needed to shorten the arc of the heatsinks a little bit.

![](/pellatis/img/extruder-heattsink-clamp.JPG)

![](/pellatis/img/extruder-heatsink-bend.JPG)

![](/pellatis/img/extruder-heatsink-wings.JPG)

### Shortening the Clamping Arc

![](/pellatis/img/extruder-heatsink-chop.JPG)

### Drilling Holes and Breaking Edges

![](/pellatis/img/extruder-heatsink-drill.JPG)

### Test Mount

![](/pellatis/img/extruder-heatsinks-on-barrel.JPG)

