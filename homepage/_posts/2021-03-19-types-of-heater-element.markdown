---
layout: post
title:  "Choosing Type of Heater Element"
date:   2021-03-19 10:02:14 +0100
categories: dev update
---
_by tobben_

Pellatis design has set out as a side project for both of us,
and the design philosophy we tend to arrive at in discussions is
"test if the simplest solutions could work first",
with "simplest" and "work" both defined by our subjective opinion.

We don't have experience with pellet extrusion from before,
so we started by ordering the parts that most pellet extruders
have. The ones that get mentioned in many a pellet extruder tutorial ([example](https://www.sciencedirect.com/topics/engineering/screw-extruder)):

![Pellet Extruer Basic Parts Sketch](/pellatis/img/pellet_extruder_basic_parts.png)

 * Barrel
 * Auger/Screw
 * Nozzle/Die
 * Variable speed drive
 * Band heaters

However, the latter of these parts gave us a bit of trouble already in the CAD phase:

![Early Pellatis CAD screenshot](/pellatis/img/a_little_bit_tight_w_arrows.png)

We knew we wanted three heater zones, following conventional pellet extruder design norms:

 * Feed zone (Melt enough for air to escape, but not so much that plastic melts in the feed throat)
 * Transition/mixing zone (Most of the heating happens here)
 * Metering zone (Maintain temperature here)

But we looked at that CAD and we thought: Isn't that... a little bit... tight?
Look for example at the cooling ribs.
They need to be hack-sawed to open up the pellet inlet.
The [Bloft Bad Ass Pellet Extruder](https://github.com/Bloft-Design-Lab/Bad-Ass-Pellet-Extruder) is our source for the heat sink CAD and hack saw solution shown above.

And look at the squished in top thermistor.
It needs to be above the topmost heater somehow, since we need to know the feed zone temperature.

## Doing Something About That Space Constraint

The bottom thermistor in the image above already hints at what we could do to solve this.
The nozzle of the Pellatis is big enough that we may simply shift
the metering zone down into the nozzle.
We can't wrap our nozzle in spiral heaters, like [Dyze does]((https://dyzedesign.com/pulsar-pellet-extruder/)), but we can swap a band heater for two good old cartridge heaters instead.

![Heater Cartridge Driven Metering Zone](/pellatis/img/no_tight_anymore_w_arrows.png)

The metering zone only maintains temperature, which doesn't require all that much power anyways.
Using two mirrored cartridges partly mitigates the issue of uneven heating,
which is a common challenge for cartridge driven heating solutions.

We'll be using a 24V power supply, which will drive two
[12V heater cartridges](https://e3d-online.com/collections/printer-parts-electrical/products/high-precision-heater-cartridges)
connected in series.

It's important that the cartridges have highly precise dimensions,
since getting a good enough fit (= large enough heat transfer area)
is another common challenge in any cartridge driven heating solution.

With all that said; we haven't tested this in practice yet.
But is sure looks better in CAD.
