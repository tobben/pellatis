---
layout: post
title:  "First aluminum parts machined"
date:   2021-03-04 10:02:14 +0100
categories: dev update
---
_by Jens Dyvik_

![Adapter plate on gearbox](/pellatis/img/apapter_plate_on_nrmw30_gearbox.JPG)

We have machined the first test parts for the extruder at [Fellesverkstedet](https://www.fellesverkstedet.no/facilities), our local Fab Lab in Oslo. We used ShopBot PRS alpha 96-48 and some leftover 10mm aluminum parts.

We have decided to test a cheeky and simple design principle; we used a cheap NMRW30 gearbox not only for gearing, but as the frame of extruder. So all parts mount to the gearbox, and the gearbox mounts to a machine. We hope and assume that the cast aluminum has enough rigidity for this. We are also attempting to use the bearing of the gearbox to absorb the load of the auger, even though it is designed for radial loads, not axial. If we get in trouble we should be able to add a thrust bearing on the topside of the gearbox quite easily in the future.

The advantage of this current plan is that very few custom metal parts needs to be fabricated. Just one adapter plate and one bushing ring. The adapterplate bridges the tapped hole pattern on the gearbox with the hole pattern on the flange of the barrel from Robotdigg. The bushing make sure that the auger is a close to the nozzle as possible, so that we dont have an unnecessary large melt pool of plastic between the auger and the nozzle. We suspect that would create more unwanted elasticity in start and stop of extruding, and increase warm up time for the extruder.

Next up is machining holes in the barrel for the thermistors, make a hopper, and mount the hopper, heatsinks, thermistors and heatbands to the barrel.

![Making](/pellatis/img/milling_first_adapter_plate01.JPG)
_The leftover piece is a part from an 2016 machine experiment, the [award plotter](https://github.com/fellesverkstedet/fabricatable-machines-archive/blob/master/award-plotter/README.md)_

![More making](/pellatis/img/milling_first_adapter_plate02.JPG)
_Note the use of plywood washers for hold down on the MDF bed of the shopbot. It works great and wont destroy the bit of you mill into them. Unfortunately the center one did not hold up so well. We should have use tabs_

![The parts](/pellatis/img/first_adapter_plate_and_test_3D_printed_parts.JPG)
_The parts we test 3D printed first to left, our freshly machined parts to the right_

![Exploded assembly](/pellatis/img/adapter_plate_and_storebought_parts.JPG)
_Exploded assembly_

![Assembly](/pellatis/img/rough_assembly.JPG)
_Cyborg penis assembled, ahem... i mean: barrel, auger, gearbox, closed loop nema23 stepper and adapter plate_
