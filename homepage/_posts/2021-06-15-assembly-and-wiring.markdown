---
layout: post
title:  "Pellatis v0.1 assembly and wiring"
date:   2021-06-15 14:02:14 +0100
categories: dev update
---
_by Jens Dyvik_

# Assembly

![](/pellatis/img/extruder-parts-knowled-div.jpg)
*Storebought parts to the left, hacked storebought parts in the center and self made parts to the right*

![](/pellatis/img/extruder-assembly-adapter-plate.JPG)
*The adapter plate combines the hole patterns of the NMRV30 gearbox and the barrel flange*

![](/pellatis/img/extruder-assembly-adapter-plate-to-gearbox.JPG)
*The nice and naughty part of our design is that we use the cast aluminum gearbox as a the extruder frame, and the radial gearbox bearing as a thrust bearing. Testing will tell if this is smart or stupid*

![](/pellatis/img/extruder-assembly-screw-ring-and-key.JPG)
*You can buy a 5mm shaft key with most NMRV gearboxes, we made our own with an angle grinder. The spacer ring is for controlling the position of the screw in the barrel*

![](/pellatis/img/extruder-assembly-screw-ring-and-key-in-barrel.JPG)
*Screw, key and spacer ring in barrel*

![](/pellatis/img/extruder-assembly-barrel-to-adapter-plate.JPG)
*Inserting the screw into the gearbox*

![](/pellatis/img/extruder-assembly-barrel-to-adapter-plate-screws.JPG)
*Screws screws screws*

![](/pellatis/img/extruder-heatsinks-and-screws.JPG)
*The hacked heatsinks and mounting hardware*

![](/pellatis/img/extruder-heatsinks-and-and-band-heaters.JPG)
*Heatsinks and band heater mounted to barrel*

![](/pellatis/img/extruder-assembly-gaskets.JPG)
*Hopper adapter and (maybe overkill) insulating gaskets*

![](/pellatis/img/extruder-assembly-shaft-adapter-on-motor.JPG)
*We bought a 11mm to 8mm shaft adapter together with the gearbox. Handy!*

![](/pellatis/img/extruder-assembly-motor-to-gearbox.JPG)
*Closed loop stepper mounted to the gearbox. The orientation of the motor/gearbox in relation to the barrel opening is up to you.*

![](/pellatis/img/extruder-inserting-heater-cartridges.JPG)
*With a little bit of reaming with got a nice pressfit for the heater cartridges, we did drill the holes about 1mm too shallow, but it should be ok. One of the holes for the thermistors we reamed too much and had to shim with some aluminum cooking foil (not great)*

![](/pellatis/img/extruder-assembly-nozzle.JPG)
*Nozzle still missing the thermistor*

![](/pellatis/img/extruder-assembly-tightening-nozzle.JPG)
*Tightening the nozzle with a size 30 spanner. Be careful not to damage the thermistor or heater cartridges.*

![](/pellatis/img/extruder-assembled.JPG)
*Pellatis v0.1 ready for wiring*

# Wiring

We CNC milled one of my open source [parametric sawhorse](https://github.com/JensDyvik/parametric-sawhorse/blob/main/README.md) to use as a test rig before mounting the extruder to a machine.

![](/pellatis/img/extruder-assembly-wiring.JPG)
*Components from left to right: 24V power supply, E-Stop (front side), circuit breaker with ground fault detection, solid state relays and Duet 2 WiFi.*

![](/pellatis/img/extruder-assembly-wiring-front.JPG)
*Wring done and ready for testing. we later added some strain relief to reduce the chances of damaging cartridge wires. In later iterations we will add insulation and a heat shield.*

# Config

Here is a dump of our current config for the Duet 2 wifi:


```
; Configuration file for Duet WiFi (firmware version 3)
; executed by the firmware on start-up
;
; generated by RepRapFirmware Configuration Tool v3.2.3 on Fri May 28 2021 15:23:17 GMT+0200 (Central European Summer Time), with modifactions by Jens Dyvik and Torbjorn Ludvigsen

; General preferences
G90                                                                     ; send absolute coordinates...
M83                                                                     ; ...but relative extruder moves
M550 P"Pellatis"                                                      ; set printer name

; Network
M552 S1                                                                 ; enable network
M586 P0 S1                                                              ; enable HTTP
M586 P1 S0                                                              ; disable FTP
M586 P2 S0                                                              ; disable Telnet

; Drives
M569 P0 S1                                                              ; physical drive 0 goes forwards
M569 P1 S1                                                              ; physical drive 1 goes forwards
M569 P2 S1                                                              ; physical drive 2 goes forwards
M569 P3 S1                                                              ; physical drive 3 goes forwards
M569 P5 S1 T5                                                           ; physical drive 5 goes forwards
M569 P6 S1 T5                                                           ; physical drive 6 goes forwards
M569 P8 S1 T5                                                           ; physical drive 8 goes forwards
M569 P7 S1 T5                                                           ; physical drive 7 goes forwards
M584 X0 Y1 Z2 E5                                        	        ; set drive mapping
M350 X16 Y16 Z16 E1 I1                          	        	; configure microstepping with interpolation
M92 X80.00 Y80.00 Z80.00 E41.75  				        ; set steps per mm
M566 X60.00 Y60.00 Z60.00 E120.00     			                ; set maximum instantaneous speed changes (mm/min) (extruder default was 120)
M203 X6000.00 Y6000.00 Z180.00 E15000.00 				; set maximum speeds (mm/min) (extruder default was 1200)
M201 X500.00 Y500.00 Z20.00 E200.00 			       	      ; set accelerations (mm/s^2)  (extruder default was 250)
M906 X800 Y800 Z800 E800 I30			                      ; set motor currents (mA) and motor idle factor in per cent
M84 S30                                                                 ; Set idle timeout

; Axis Limits
M208 X0 Y0 Z0 S1                                                        ; set axis minima
M208 X230 Y210 Z200 S0                                                  ; set axis maxima

; Endstops
M574 X2 S1 P"xstop"                                                     ; configure active-high endstop for high end on X via pin xstop
M574 Y2 S1 P"ystop"                                                     ; configure active-high endstop for high end on Y via pin ystop
M574 Z2 S2                                                              ; configure Z-probe endstop for high end on Z

; Z-Probe
M558 P5 C"^zprobe.in" H5 F120 T6000                                     ; set Z probe type to switch and the dive height + speeds
G31 P500 X0 Y0 Z0                                                       ; set Z probe trigger value, offset and trigger height
M557 X15:215 Y15:195 S20                                                ; define mesh grid

; Heaters
M140 H-1                                                                ; disable heated bed (overrides default heater mapping)
M308 S0 P"bedtemp" Y"thermistor" T100000 B4725 C7.06e-8                 ; configure sensor 0 as thermistor on pin bedtemp
M950 H0 C"bedheat" T0 Q10                                                  ; create nozzle heater output on bedheat and map it to sensor 0
M307 H0 B0 S1.00                                                        ; disable bang-bang mode for heater  and set PWM limit
M143 H0 S260                                                            ; set temperature limit for heater 0 to 260C
M308 S1 P"e0temp" Y"thermistor" T100000 B4725 C7.06e-8                  ; configure sensor 1 as thermistor on pin e0temp
M950 H1 C"e0heat" T1 Q2                                                   ; create nozzle heater output on e0heat and map it to sensor 1
M307 H1 B0 S1.00                                                        ; disable bang-bang mode for heater  and set PWM limit
M143 H1 S260                                                            ; set temperature limit for heater 1 to 260C
M308 S2 P"e1temp" Y"thermistor" T100000 B4725 C7.06e-8                  ; configure sensor 2 as thermistor on pin e1temp
M950 H2 C"e1heat" T2 Q2                                                    ; create nozzle heater output on e1heat and map it to sensor 2
M307 H2 B0 S1.00                                                        ; disable bang-bang mode for heater  and set PWM limit
M143 H2 S260                                                            ; set temperature limit for heater 2 to 260C

; Fans
;M950 F0 C"fan0" Q500                                                    ; create fan 0 on pin fan0 and set its frequency
;M106 P0 S1 H2 T24                                                       ; set fan 0 value. Thermostatic control is turned on

; Tools
M563 P0 D0 H0:1:2 F0 L1                                                ; define tool 0
G10 P0 X0 Y0 Z0                                                         ; set tool 0 axis offsets
G10 P0 R0 S0                                                            ; set initial tool 0 active and standby temperatures to 0C

; Custom settings are not defined

; Miscellaneous
T0                                                                      ; select first tool

;PID tuning values

M307 H0 R1.117 C122.3 D5.77 S1.00 V23.9
M307 H1 R0.325 C1052.2 D30.61 S1.00
M307 H2 R0.233 C767.0 D53.04 S1.00

; Cold extrusions
M302 S130 R130
```







