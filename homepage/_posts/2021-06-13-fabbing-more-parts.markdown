---
layout: post
title:  "Fabbing More Custom Parts"
date:   2021-06-13 16:02:14 +0100
categories: dev update
---
_by Jens Dyvik_

### Hopper Adapter for Juice Bottle

Cutting off the bottom of a plastic bottle is an easy way to get a transparent cylindrical part *(so we can see when to refill pellets, later we will add autofeeding)*. Reverse engineering the thread and modelling it in Freecad was a fun challenge.

![](/pellatis/img/hopper-thread-angle-trick.PNG)
*The thread angles are optimized for FFF printing.*

![](/pellatis/img/extruder-hopper-adapter-thread-detail.PNG)
*Design of the first version.*

![](/pellatis/img/hopper-adapter-print-with-suppport.JPG)
*Print with supports on build plate only did the trick.*

![](/pellatis/img/hopper-adapter-with-cut-juice-bottle.JPG)
*Test assembly.*

### Insulating Gasket to Protect the Hopper Adapter From a Potentially Overheated Barrel Opening

We have a hunch that a gasket won't be needed, but made two just in case.

![](/pellatis/img/extruder-isoltating-gasket-milling.JPG)
*We milled the gaskets from some leftover 0.7mm Formica (phenolic resin and paper composite).*

